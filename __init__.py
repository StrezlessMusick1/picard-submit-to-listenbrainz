PLUGIN_NAME = "Submit to ListenBrainz"
PLUGIN_AUTHOR = "Flaky"
PLUGIN_DESCRIPTION = "Manually submit listens of music to ListenBrainz."
PLUGIN_VERSION = '0.3'
PLUGIN_API_VERSIONS = ['2.2']
PLUGIN_LICENSE = "MIT"

from picard import config, log
from picard.album import Album, Track
# TODO standalone recording submission - import register_track_action
from picard.ui.itemviews import (BaseAction,
                                 register_album_action,
                                 register_track_action
                                 )
from picard.ui.options import OptionsPage, register_options_page

from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtCore import Qt, QDateTime

from .ui_config import SubmitToListenBrainzOptionsUI
from .ui_submit import SubmitWindow
from .listenbrainz import ListenBrainzInstance, TimeTravellerError

config.Option("setting", 'listenbrainz_id', '')

listenbrainz_id = config.setting['listenbrainz_id']
listenbrainz_host = "api.listenbrainz.org"
listenbrainz = ListenBrainzInstance(listenbrainz_host, listenbrainz_id)


class SubmitListensDialog(QDialog):

    def __init__(self, data):
        super().__init__()
        self.data = data
        self.just_finished = True
        self.ui = SubmitWindow()
        self.ui.radiobutton_now.toggled.connect(self.change_mode)
        self.ui.radiobutton_past.toggled.connect(self.change_mode)
        self.ui.submit_button.clicked.connect(self.on_submit_button_clicked)
        if isinstance(data, Album):
            self.ui.prepare_label.setText(f'Submitting <b>{data.metadata["album"]}</b> by <b>{data.metadata["albumartist"]}</b>')
            for track in data.tracks:
                self.ui.track_table.add_track(track)
                if not track.metadata.length:
                    self.ui.duration_warning.show()
                self.ui.track_table.show()
                self.ui.checkbox_button_frame.show()
        elif isinstance(data, Track):
            self.ui.prepare_label.setText(f'Submitting <b>{data.metadata["title"]}</b> by <b>{data.metadata["artist"]}</b>')
            self.ui.track_table.add_track(data)
        if data.metadata["media"] == "Digital Media":
            self.ui.streaming_dropdown_frame.show()
        self.ui.exec()

    def on_submit_button_clicked(self):
        tracks = []
        # defaults to blank anyway, but will need to implement this better
        # if options for default parameters are going to be added.
        service = self.ui.streaming_dropdown.currentData(Qt.UserRole)
        start_timestamp = QDateTime.currentSecsSinceEpoch() if self.just_finished else self.ui.date_widget.dateTime().toSecsSinceEpoch()
        for row in range(self.ui.track_table.rowCount()):
            if self.ui.track_table.item(row, 0).checkState() == Qt.Checked:
                tracks.append(
                    self.ui.track_table.item(row, 0).data(Qt.UserRole)
                    )
            else:
                log.info(f"Not including {self.ui.track_table.item(row, 0).data(Qt.UserRole)}because the user had unchecked it.")
        error = QMessageBox()
        error.setStandardButtons(QMessageBox.Ok)
        error.setDefaultButton(QMessageBox.Ok)
        error.setIcon(QMessageBox.Critical)
        try:
            listenbrainz.submit_tracks(
                self.ui,
                tracks,
                start_timestamp,
                self.just_finished,
                service
            )
            # TODO consolidate tracks, start timestamp, just finished into one dict file.
        except TimeTravellerError:
            error.setText("You are trying to submit listens with timestamps from the future. Double-check your initial timestamp in case you forgot to change it.")
            error.exec_()
        except TypeError as e:
            error.setText(str(e))
            error.exec_()
        except Exception as e:
            log.error(e)
            error.setText("A general error has occurred. Check the logs for more information.")
            error.exec_()

    def change_mode(self):
        radiobutton = self.sender()
        if radiobutton.isChecked():
            if radiobutton.text() == "Now (just finished)":
                self.ui.date_frame.hide()
                self.just_finished = True
            else:
                self.ui.date_frame.show()
                self.just_finished = False


class SubmitReleaseListensMenu(BaseAction):
    NAME = 'Submit release listens to ListenBrainz'

    def callback(self, objs):
        handle_submit_ui(objs)

class SubmitRecordingListensMenu(BaseAction):
    NAME = 'Submit recording listens to ListenBrainz'

    def callback(self, objs):
        handle_submit_ui(objs)

class SubmitToListenBrainz_OptionsPage(OptionsPage):
    NAME = "submit_to_listenbrainz"
    TITLE = "Submit to ListenBrainz"
    PARENT = "plugins"
    HELP_URL = "https://gitlab.com/Flaky/picard-submit-to-listenbrainz/"

    def __init__(self, parent=None):
        super().__init__()
        self.ui = SubmitToListenBrainzOptionsUI(self)

    def load(self):
        self.ui.userauth_textbox.setText(config.setting['listenbrainz_id'])

    def save(self):
        config.setting['listenbrainz_id'] = self.ui.userauth_textbox.text().strip()
        listenbrainz.user_id = config.setting['listenbrainz_id']


def handle_submit_ui(objs):
    error = QMessageBox()
    error.setStandardButtons(QMessageBox.Ok)
    error.setDefaultButton(QMessageBox.Ok)
    if config.setting['listenbrainz_id']:
        if (len(objs) == 1):
            item = objs[0]
            if isinstance(item, Album):
                if len(item.tracks) != 0:
                    if item.metadata["album"] != "[standalone recordings]":
                        SubmitListensDialog(item)
                    else:
                        error.setIcon(QMessageBox.Information)
                        error.setText("If you want to submit a standalone recording, right click the recording itself.")
                        error.exec_()
            elif isinstance(item, Track):
                 SubmitListensDialog(item)
        else:
            error.setIcon(QMessageBox.Critical)
            error.setText("Only one album/recording at a time should be submitted.")
            error.exec_()
    else:
        error.setIcon(QMessageBox.Information)
        error.setText("You have not configured your user token. Go into Options > Options, then Plugins > Submit to ListenBrainz to configure it.")
        error.exec_()


register_options_page(SubmitToListenBrainz_OptionsPage)
register_album_action(SubmitReleaseListensMenu())
register_track_action(SubmitRecordingListensMenu())

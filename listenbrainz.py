import json
import re
from PyQt5.QtNetwork import (
    QNetworkAccessManager,
    QNetworkRequest,
    QNetworkReply
    )

from picard import log
from picard.track import Track
from PyQt5.QtCore import QDateTime, QUrl
from PyQt5.QtWidgets import QMessageBox
from .artist_grab import grab_artist_tag

lb_add_multi_bindings = [
    # ListenBrainz parameter, metadata tag
    ("artist_mbids", "musicbrainz_artistid"),
    ("work_mbids","musicbrainz_workid"),
    ]

lb_add_single_bindings = [
    # I could possibly combine these, but not right now.
    ("release_group_mbid","musicbrainz_releasegroupid"),
    ("release_mbid","musicbrainz_albumid"),
    ("recording_mbid","musicbrainz_recordingid"),
    ("track_mbid","musicbrainz_trackid"),
    ("tracknumber", "~musicbrainz_tracknumber"),
    # ("isrc","isrc"),
    ]


class TimeTravellerError(Exception):
    """
    Raised when the ListenBrainz instance is trying to import a timestamp
    from the future.
    """
    pass


class ListenBrainzInstance:
    # doing it DIY as not everyone will have Pylistenbrainz installed.
    def __init__(self, host, user_id):
        self.user_id = user_id
        self.host = host
        self.network = QNetworkAccessManager()
        self.submit_ui = None

    def submit_tracks(self,
                      submit_ui,
                      track_list,
                      start_timestamp,
                      just_finished,
                      service=""):

        self.submit_ui = submit_ui

        if (self.user_id and self.host
             and isinstance(track_list, list) and len(track_list) > 0):
            if just_finished:
                track_list = reversed(track_list)

            submit_json = {
                    "listen_type": "import",
                    "payload": []
                }
            current_timestamp = start_timestamp

            for track in track_list:
                submit_payload_data = {  # only definite, filled data here.
                    "track_metadata": {
                        "additional_info": {
                            "submission_client":
                                "Submit to ListenBrainz Plugin for Picard",
                            "submission_client_version": "0.3"
                            },
                        "artist_name": grab_artist_tag(track),
                        }
                    }

                if hasattr(track.metadata, "length"):
                    if track.metadata.length != 0:
                        # Picard needs millisecond length
                        length_sec = track.metadata.length / 1000
                        if not just_finished:
                            if ((current_timestamp + length_sec) >
                                    QDateTime.currentSecsSinceEpoch()):
                                raise TimeTravellerError
                            else:
                                current_timestamp += length_sec
                                submit_payload_data["track_metadata"]["additional_info"]["duration_ms"] = track.metadata.length
                                submit_payload_data["listened_at"] = current_timestamp
                        else:
                            # when the user had just finished listening.
                            submit_payload_data["track_metadata"]["additional_info"]["duration_ms"] = track.metadata.length
                            submit_payload_data["listened_at"] = current_timestamp
                            current_timestamp -= length_sec
                    else:
                        # TODO allow timestamp editing
                        submit_payload_data["listened_at"] = current_timestamp

                    submit_payload_data["track_metadata"]["track_name"] = track.metadata["title"] if track.metadata["title"] else "[unknown]"

                    if (track.metadata["album"]
                            and track.metadata["album"] != "[standalone recordings]"):
                        submit_payload_data["track_metadata"]["release_name"] = track.metadata["album"]

                    if service: submit_payload_data["track_metadata"]["additional_info"]["music_service"] = service

                    for binding in lb_add_single_bindings:
                        if track.metadata[binding[1]]:
                            submit_payload_data["track_metadata"]["additional_info"][binding[0]] = track.metadata[binding[1]]

                    for binding in lb_add_multi_bindings:
                        # splits mostly based on Picard's standards.
                        # most people will be splitting that way... I bloody hope.
                        if track.metadata[binding[1]]:
                            split_data = re.split(r'; |, |/| / ', track.metadata[binding[1]])
                            submit_payload_data["track_metadata"]["additional_info"][binding[0]] = split_data

                submit_json["payload"].append(submit_payload_data)

            request = QNetworkRequest(
                QUrl(f'https://{self.host}/1/submit-listens')
                    )
            request.setRawHeader('Authorization'.encode(),
                                    f'Token {self.user_id}'.encode())
            request.setHeader(QNetworkRequest.KnownHeaders.ContentTypeHeader,
                                'application/json')
            # TODO try QtNetwork again.
            self.response = self.network.post(request,
                                              json.dumps(submit_json).encode())
            self.response.finished.connect(self.on_listen_submit_attempt)
        else:
            if not self.user_id or not self.host:
                raise TypeError("Missing user or host information, cannot connect to ListenBrainz.")
            elif (len(track_list) < 1):
                raise TypeError("No recordings were selected. Please select at least one recording before submitting.")

    def on_listen_submit_attempt(self):
        log.info("on_listen_submit_attempt triggered")
        response_data = bytes(self.response.readAll()).decode()
        error_msg = QMessageBox()
        error_msg.setIcon(QMessageBox.Critical)
        error_msg.setStandardButtons(QMessageBox.Ok)
        error_msg.setDefaultButton(QMessageBox.Ok)
        if self.response.error() == QNetworkReply.NoError:
            lb_response = json.loads(response_data)
            if lb_response["status"] == "ok":
                self.submit_ui.close()
            else:
                error_msg.setText(f"Error submitting this release to ListenBrainz.\n\nJSON information from ListenBrainz: {str(lb_response)}")
                error_msg.exec_()
        else:
            error = self.response.error()
            error_text = ""
            # see: https://doc.qt.io/qt-6/qnetworkreply.html
            # this'd be better handled with case/switch tbh
            # need to see what python ver that Win Picard is on
            if error == QNetworkReply.NetworkError.AuthenticationRequiredError:
                error_text = "A 401 Unauthorized error was reported. Check that you have entered your user ID in correctly in the settings."
            elif error == QNetworkReply.NetworkError.ConnectionRefusedError:
                error_text = "The connection was refused. This is typically if an invalid host was specified."
            elif error == QNetworkReply.NetworkError.HostNotFoundError:
                error_text = "The specified host was not found. This is typically if an invalid host was specified."
            elif error == QNetworkReply.NetworkError.ProtocolInvalidOperationError:
                error_text = "The data sent to ListenBrainz was invalid. Raise an issue at https://gitlab.com/Flaky/picard-submit-to-listenbrainz"
            elif error == QNetworkReply.NetworkError.ServiceUnavailableError:
                # TODO try to resubmit several times before showing this error.
                error_text = "The service was reported as unavailable. Try to re-submit again."
            else:
                error_text = f"A network error has occured.\n\nQt error code: {str(error)}"
            error_msg.setText(f"Error submitting this release to ListenBrainz.\n\n{error_text}")
            error_msg.exec_()

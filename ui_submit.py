from PyQt5.QtWidgets import (
    QDialog,
    QHeaderView,
    QAbstractItemView,
    QAbstractScrollArea,
    QLayout,
    QVBoxLayout,
    QHBoxLayout,
    QFrame,
    QLabel,
    QRadioButton,
    QPushButton,
    QComboBox,
    QDateTimeEdit,
    QTableWidget,
    QTableWidgetItem,
    QSizePolicy,

    )

from PyQt5.QtCore import Qt, QDateTime
from picard.track import Track
from .artist_grab import grab_artist_tag

# TODO: streaming services here
streaming_services = [
    ("[other/do not fill in]", ""),
    ("Apple Music", "music.apple.com"),
    ("Bandcamp", "bandcamp.com"),
    ("Deezer", "deezer.com"),
    ("Google Play Music", "play.google.com"),
    ("Internet Archive", "archive.org"),
    ("Jamendo Music", "jamendo.com"),
    ("Qobuz", "qobuz.com"),
    ("SoundCloud", "soundcloud.com"),
    ("Spotify", "spotify.com"),
    ("Tidal", "tidal.com"),
    ("YouTube", "youtube.com"),
    ("YouTube Music", "music.youtube.com")
    # also worth adding? qobuz
    ]


class SubmitWindow(QDialog):

    def on_checkbox_button_pressed(self):
        self.track_table.toggle_check_all(True)

    def on_uncheckbox_button_pressed(self):
        self.track_table.toggle_check_all(False)

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Submit to ListenBrainz Plugin")

        self.main_container = QVBoxLayout()

        self.top_bit = QVBoxLayout()
        self.prepare_label = QLabel()
        self.top_bit.addWidget(self.prepare_label)

        self.timestamp_choice_frame = QFrame()
        self.timestamp_choice_layout = QHBoxLayout()
        self.radiobutton_now = QRadioButton()
        self.radiobutton_now.setText("Now (just finished)")
        self.radiobutton_now.setChecked(True)
        self.radiobutton_past = QRadioButton()
        self.radiobutton_past.setText("Past listen")
        self.timestamp_choice_layout.addWidget(self.radiobutton_now)
        self.timestamp_choice_layout.addWidget(self.radiobutton_past)
        self.timestamp_choice_layout.addStretch()
        self.timestamp_choice_frame.setLayout(self.timestamp_choice_layout)
        self.top_bit.addWidget(self.timestamp_choice_frame)

        self.date_frame = QFrame()
        self.date_layout = QHBoxLayout()
        self.date_layout.setSizeConstraint(QLayout.SetFixedSize)
        self.date_label = QLabel("Time/date of when you started listening: ")
        self.date_widget = QDateTimeEdit()
        self.date_widget.setDateTime(QDateTime.currentDateTime())
        self.date_widget.setCalendarPopup(True)
        self.date_widget.setDisplayFormat("yyyy/MM/dd HH:mm:ss")
        self.date_layout.addWidget(self.date_label)
        self.date_layout.addWidget(self.date_widget)
        self.date_frame.setLayout(self.date_layout)
        self.date_frame.setHidden(True)
        self.top_bit.addWidget(self.date_frame)

        self.checkbox_button_frame = QFrame()
        self.checkbox_button_layout = QHBoxLayout()
        self.checkbox_button_layout.setSizeConstraint(QLayout.SetFixedSize)
        self.checkall_button = QPushButton()
        self.checkall_button.setText("Check all")
        self.checkall_button.setSizePolicy(QSizePolicy.Policy.Maximum,
                                           QSizePolicy.Policy.Maximum)
        self.checkall_button.clicked.connect(self.on_checkbox_button_pressed)
        self.uncheckall_button = QPushButton()
        self.uncheckall_button.setText("Uncheck all")
        self.uncheckall_button.setSizePolicy(QSizePolicy.Policy.Maximum,
                                             QSizePolicy.Policy.Maximum)
        self.uncheckall_button.clicked.connect(
            self.on_uncheckbox_button_pressed
            )
        self.checkbox_button_layout.addWidget(self.checkall_button)
        self.checkbox_button_layout.addWidget(self.uncheckall_button)
        self.checkbox_button_layout.addStretch()
        self.checkbox_button_frame.setLayout(self.checkbox_button_layout)
        self.checkbox_button_frame.setHidden(True)
        self.top_bit.addWidget(self.checkbox_button_frame)

        self.main_container.addLayout(self.top_bit)
        self.main_container.addSpacing(10)

        self.track_table = TrackTableView()
        self.track_table.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.track_table.setHidden(True)
        self.main_container.addWidget(self.track_table)
        self.streaming_dropdown_frame = QFrame()
        self.streaming_dropdown_layout = QHBoxLayout()
        self.streaming_label = QLabel()
        self.streaming_dropdown_layout.addWidget(self.streaming_label)
        self.streaming_label.setText(
            "Streaming service used for digital media release: "
            )
        self.streaming_dropdown = QComboBox()
        for service in streaming_services:
            self.streaming_dropdown.addItem(service[0], service[1])
        self.streaming_dropdown_layout.addWidget(self.streaming_dropdown)
        self.streaming_dropdown_frame.setLayout(self.streaming_dropdown_layout)
        self.streaming_dropdown_frame.setHidden(True)
        self.main_container.addWidget(self.streaming_dropdown_frame)

        self.bottom_bit = QVBoxLayout()
        self.duration_warning = QLabel()
        self.duration_warning.setWordWrap(True)
        self.duration_warning.setText("<b>WARNING:</b> This release contains recordings with no duration. This will currently import all of the tracks as if you had listened to it all in one go. If you care about accuracy in your listen history, <b>do not import this release just yet.</b> In the future, you will be able to input the timestamps.")
        self.duration_warning.setHidden(True)
        self.bottom_bit.addWidget(self.duration_warning)
        self.submit_button = QPushButton()
        self.submit_button.setText("Submit to ListenBrainz!")
        # TODO disable when all boxes unchecked.
        self.bottom_bit.addWidget(self.submit_button)
        self.main_container.addLayout(self.bottom_bit)

        self.setLayout(self.main_container)
        # TODO handle standalone tracks here

class TrackTableView(QTableWidget):
    def __init__(self):
        super().__init__()
        QTableWidget.__init__(self)
        self.setColumnCount(3)
        self.setHorizontalHeaderLabels(['Recording', 'Artist', 'Length'])
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.setMinimumWidth(500)
        self.setSelectionMode(QAbstractItemView.NoSelection)

    def add_track(self, track: Track):
        if isinstance(track, Track):
            row = self.rowCount()
            self.setRowCount(row + 1)
            track_row_data = [
                track.metadata["title"],
                grab_artist_tag(track),
                track.metadata["~length"]
                ]
            column = 0
            for item in track_row_data:
                cell = QTableWidgetItem(item)
                if column == 0:
                    cell.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
                    cell.setData(Qt.UserRole, track)
                    cell.setCheckState(Qt.Checked)
                self.setItem(row, column, cell)
                column += 1
        else:
            raise TypeError("Not a Track object.")

    def toggle_check_all(self, checked: bool):
        for row in range(self.rowCount()):
            checkbox = self.item(row, 0)
            if checked:
                checkbox.setCheckState(Qt.Checked)
            else:
                checkbox.setCheckState(Qt.Unchecked)
